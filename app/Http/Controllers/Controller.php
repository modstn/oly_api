<?php

namespace App\Http\Controllers;

use App\Player;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;

use Illuminate\Http\Request;

class Controller extends BaseController
{
    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;

        $rules = $this->generateRules();

        $this->validate($this->request, $rules);
    }

    private function generateRules()
    {
        $max_win_amount = 20000;

        $odds = [];

        $rules = [
            'player_id' => 'required|integer',
            'selections' => [
                'array',
                function ($attribute, $value, $fail) {
                    $selections_amount = count($this->request[$attribute]);
                    if ($selections_amount < 1) {
                        $fail(
                            [
                                'code' => 4,
                                'message' => 'Minimum number of selections is 1'
                            ]
                        );
                    }
                    if ($selections_amount > 20) {
                        $fail(
                            [
                                'code' => 5,
                                'message' => 'Maximum number of selections is 20'
                            ]
                        );
                    }
                },
                'required',
            ],
            'selections.*.id' => [
                'required',
                'integer',
            ],
            'selections.*.odds' => [
                'required',
                'string',
                function ($attribute, $value, $fail) use (&$odds) {
                    $odds[] = $value;
                }
            ],
            'stake_amount' => [
                'required',
                'string',
                function ($attribute, $value, $fail) use ($max_win_amount) {
                    if ($value < 0.3) {
                        $fail(
                            [
                                'code' => 2,
                                'message' => 'Minimum stake amount is 0.3',
                            ]
                        );
                    }
                    if ($value > 10000) {
                        $fail(
                            [
                                'code' => 3,
                                'message' => 'Maximum stake amount is 10000',
                            ]
                        );
                    }
                    if (stripos($value, '.') !== false) {
                        $number = explode('.', $value);
                        if (!empty($number[1]) && strlen($number[1]) > 2) {
                            $fail(
                                [
                                    'code' => 0,
                                    'message' => 'max number of numbers after dot is 2',
                                ]
                            );
                        }
                    }

                    $amount = $value;
                    foreach ($this->request['selections'] as $selection) {
                        $amount *= $selection['odds'];
                    }

                    if ($amount > $max_win_amount) {
                        $fail(
                            [
                                'code' => 9,
                                'message' => 'Maximum win amount is 20000'
                            ]
                        );
                    }

                    $player = Player::find($this->request['player_id']);
                    if (!empty($player)) {
                        $balance = $player->balance;
                    } else {
                        $balance = 1000;
                    }
                    if ($value > $balance) {
                        $fail(
                            [
                                'code' => 11,
                                'message' => 'Insufficient balance'
                            ]
                        );
                    }
                }
            ],
        ];

        return $rules;
    }

    protected function buildFailedValidationResponse(Request $request, array $errors)
    {
        $data = $request->toArray();
        $data['errors'] = [];
        foreach ($errors as $error_key => $error) {
            if (stripos($error_key, '.') !== false) {
                $error_data = explode('.', $error_key);
                if (count($error_data) == 3) {
                    $data[$error_data[0]][$error_data[1]]['errors'] = $error;
                } else {
                    $data['errors'] = [
                        'code' => 0,
                        'message' => "Unknown error",
                    ];
                }

                unset($errors[$error_key]);
            } else {
                $data['errors'] = array_merge($data['errors'], $error);
            }
        }
        return $data;
    }


}
