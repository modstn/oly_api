<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BetApiInitialMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player', function ($table) {
            $table->increments('id');
            $table->float('balance');
            $table->timestamps();
        });
        Schema::create('balance_transaction', function ($table) {
            $table->increments('id');
            $table->integer('player_id');
            $table->integer('bet_id');
            $table->float('amount');
            $table->float('amount_before');
            $table->timestamps();
        });
        Schema::create('bet', function ($table) {
            $table->increments('id');
            $table->integer('player_id');
            $table->float('stake_amount');
            $table->timestamps();
        });
        Schema::create('bet_selections', function ($table) {
            $table->increments('id');
            $table->integer('bet_id');
            $table->integer('selection_id');
            $table->double('odds', 5, 3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player');
        Schema::dropIfExists('balance_transaction');
        Schema::dropIfExists('bet');
        Schema::dropIfExists('bet_selections');
    }
}
