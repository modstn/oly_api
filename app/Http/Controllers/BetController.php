<?php

namespace App\Http\Controllers;


use App\BalanceTransaction;
use App\Bet;
use App\BetSelections;
use App\Player;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class BetController extends Controller
{
    private $player;
    private $bet;
    private $betSelection;
    private $balanceTransaction;

    public function __construct(
        Request $request,
        Player $player,
        Bet $bet,
        BetSelections $betSelections,
        BalanceTransaction $balanceTransaction
    ) {
        parent::__construct($request);

        $this->player = $player;
        $this->bet = $bet;
        $this->betSelection = $betSelections;
        $this->balanceTransaction = $balanceTransaction;
    }

    /**
     * Bet process
     *
     * @return array
     */
    public function addBet()
    {
        $this->validateAddBet();

        $player = $this->getPlayer($this->request['player_id']);

        $bet = $this->createABet($player->id, $this->request['stake_amount']);

        $this->createSelections($bet->id, $this->request['selections']);

        $this->balanceTransaction($bet->id);

        return [];
    }

    /**
     * Perform balance transaction and update player balance.
     *
     * @param $bet_id
     */
    private function balanceTransaction($bet_id)
    {
        $bet = Bet::find($bet_id);

        $player = Player::find($bet->player_id);

        $bet_selections = BetSelections::where(['bet_id' => $bet_id])->get();

        /**
         * implement "random" win/lose aspect
         */
        $win_lose = time() % 2;

        $player_balance = $player->balance;

        $stake_amount = $bet->stake_amount;

        if ($win_lose) {
            //win

            $win_amount = $stake_amount;

            foreach ($bet_selections as $bet_selection) {
                $win_amount *= $bet_selection->odds;
            }

            $this->createBalanceTransactionInsert($bet->id, $player->id, $win_amount,$player_balance);

            $player->balance = $player_balance + $win_amount;
            $player->update();

        } else {
            //lose

            $this->createBalanceTransactionInsert($bet->id, $player->id, $stake_amount * (-1),$player_balance);

            $player->balance = $player_balance - $stake_amount;
            $player->update();
        }

    }


    /**
     * Create a database entry for balance transaction
     *
     * @param $bet_id
     * @param $player_id
     * @param $amount
     * @param $balance_before
     */
    private function createBalanceTransactionInsert($bet_id,$player_id,$amount,$balance_before){
        $balance_transaction = new BalanceTransaction();
        $balance_transaction->player_id = $player_id;
        $balance_transaction->bet_id = $bet_id;
        $balance_transaction->amount = $amount;
        $balance_transaction->amount_before = $balance_before;
        $balance_transaction->save();
    }

    /**
     * Create new bet selections for a bet.
     *
     * @param $bet_id
     * @param $selections
     * @return array
     */
    private function createSelections($bet_id, $selections)
    {
        $created_selections = [];

        foreach ($selections as $selection) {
            $c_selection = new BetSelections();
            $c_selection->bet_id = $bet_id;
            $c_selection->selection_id = $selection['id'];
            $c_selection->odds = $selection['odds'];
            $c_selection->save();
            $created_selections[] = $c_selection;
        }
        return $created_selections;
    }

    /**
     * Create new bet for player with stake_amount.
     *
     * @param $player_id
     * @param $stake_amount
     * @return Bet
     */
    private function createABet($player_id, $stake_amount)
    {
        $bet = new Bet();
        $bet->player_id = $player_id;
        $bet->stake_amount = $stake_amount;
        $bet->save();
        return $bet;
    }

    /**
     * Get or create a new player in database
     *
     * @param $player_id
     * @return Player
     */
    private function getPlayer($player_id)
    {
        $player = Player::find($player_id);
        if (empty($player)) {
            $player = new Player();
            $player->id = $player_id;
            $player->balance = 1000;
            $player->save();
        }
        return $player;
    }

    /**
     * AddBet function request validation
     */
    private function validateAddBet()
    {
        $selection_ids = [];

        $this->validate($this->request, [
            'selections.*.id' => [
                function ($attribute, $value, $fail) use (&$selection_ids) {
                    if (in_array($value, $selection_ids)) {
                        $fail(
                            [
                                'code' => 0,
                                'message' => 'Duplicate selection found',
                            ]
                        );
                    }
                    $selection_ids[] = $value;
                },
            ],
            'selections.*.odds' => [
                function ($attribute, $value, $fail) {
                    if ($value < 1) {
                        $fail(
                            [
                                'code' => 6,
                                'message' => 'Minimum odds are 1',
                            ]
                        );
                    }
                    if ($value > 10000) {
                        $fail(
                            [
                                'code' => 7,
                                'message' => 'Maximum odds are 10000',
                            ]
                        );
                    }
                    if (stripos($value, '.') !== false) {
                        $number = explode('.', $value);
                        if (!empty($number[1]) && strlen($number[1]) > 3) {
                            $fail(
                                [
                                    'code' => 0,
                                    'message' => 'max number of numbers after dot is 3',
                                ]
                            );
                        }
                    }
                },
            ]
        ]);
    }
}
